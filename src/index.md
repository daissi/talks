% Debian l10n fr

Présentations de l'équipe Française de traduction
=================================================

L'équipe Debian l10n-fr

---

Marseille - Le 25 mai 2019
---------------------------------------------

La localisation de Debian en langue française.

Supports de la présentation :

 * [Version HTML en ligne](2019-minidebconf-marseille.html)
 * [Version PDF simplifiée](2019-minidebconf-marseille.pdf)

Présentation en vidéo :

 * [Version webm](https://meetings-archive.debian.net/pub/debian-meetings/2019/miniconf-marseille/2019-05-25/french_localization.webm)
 * [Version webm (basse qualité)](https://meetings-archive.debian.net/pub/debian-meetings/2019/miniconf-marseille/2019-05-25/french_localization.webm)
 * [Version Youtube](https://www.youtube.com/watch?v=lG5n4H1gruo&list=PLYUtdmpYPTTL0KsW177xmSOJTo09yZ2gI&index=9)

Sous-titres de la vidéo :

 * [Sous-titres Français](https://meetings-archive.debian.net/pub/debian-meetings/2019/miniconf-marseille/2019-05-25/subtitles/french_localization.fr.srt)
 * [Sous-titres Anglais](https://meetings-archive.debian.net/pub/debian-meetings/2019/miniconf-marseille/2019-05-25/subtitles/french_localization.en.srt)

---

[Sources sur Salsa](https://salsa.debian.org/l10n-fr-team/talks)
