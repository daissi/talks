---
title: Building an attractive way in an old infra for new translators
author: Jean-Philippe MENGUAL
date: 2023, February, 5th
---

# Fast introduction

+ president of traduc.org during 3 years (2010-2013)
+ LFS French translation coordinator (2008-2016)
+ Debian contributor (2004-2019), now Debian Developer non uploading and
  get involved in the translation in the project, as well as member of the
  Community Team (to enforce the code of conduct)

# An example: Debian French l10n: a very small team

+ Jean-Pierre Giraud, Jean-Paul Guillonneau, Jean-Philippe MENGUAL are the
  three main contributors
+ bubu, Lucien contribute via reviews and translations
+ Baptiste Jammet, Thomas Vincent have contributed as translator and continue
  to help from a technical point   of view
+ Alban Vidal translated (and show me how things work and introduced me in
  the team)

# However, amazing results

+ Debian installer: 100% translated
+ Website: about 12 000 pages (99 % translated!), thanks to Christian Perrier (bubulle),
  Thomas Vincent (tvincent), and the current team
+ Debian Package Description: About 67 500 packages and 23 695 translated (35%), including 100% required and standard packages,
  96% important, less than 35% others
+ 3rd language the most translated behind Dannish and Italian teams

# Debian is one of the oldest Linux distribution alive

+ First public mail about French translation: 1998, November
+ A mail-based workflow:
  - web-pages make the work public
  - bots processing this to make it public are managed via mails

# Mail-based tools

## 

A bot on the debian-l10n-french mailing parses the messages topic to:

+ track the translations in progress
+ print the translations to be updated

So that the team is able to:

+ track the changes from our mailboxes
+ know the status of the translations
[exemple page stats pod-debconf]

# Coordination pages

##

Various tracking pages:

+ [announces on the mailing list](https://l10n.debian.org/coordination/french/fr.by_status.html)
+ [packages translated via po](https://www.debian.org/international/l10n/po/fr)
+ [packages translated via po4a](https://www.debian.org/international/l10n/po4a/fr)
+ [debconf screens](https://www.debian.org/international/l10n/po-debconf/fr)
+ [website](https://www.debian.org/devel/website/stats/fr)

# Very various formats of what we translate

+ the packages description database
+ html/wml files
+ PO files

# Why changing is difficult

+ should we change?
+ few new contributors
+ very difficult to become attractive as changing processes is very long for a
  so small team
+ when people come, they don't leave anymore
+ moving may take years (the migration of the packages description
  system server to the official Debian infra took years and exhausted several
  contributors)
+ Could we use mainstream tools: I don't know, but would not be easy. All the
  more as they are not accessible for blind people and one of contributors is
  blind.
+ Finally, people like having files off line and working as dev workflow,
  with a responsibility of the translation by the translator instead of a full
  collective and sync work

# Instead of changing tools, how to introduce things better for newcommers?

+ creating a way for a new contributor
+ creating at each step 2 possible status

# The way to contribute at one's rhythm

## Start with wiki?

+ subscribe, take the English page, create a page, copy the English one and
  translate

## Packages description?

+ Description of **all** packages

+ Web interface (DDTSS)

+ interaction with the team which review the work and tell you the habits of the
  translation in the project

## Debconf screens

+ Configuration Debian screens printed during the installation process of some
  packages (e.g. Exim)
+ easy to discover the process but a responsibility because users will trust your
  work


# The process

+ coordination of the packages changes and posting to the mailing list a TAF
  or MAJ mail (Julien Patriarca tries doing this)
+ once the mail received, informing we work about it (ITT)
+ translating PO file and request for reviews via mails
+ testing the result
+ report a bug on the Bug Tracking System against the package with the patch
  containing the translation (up-to-date PO file)
+ informing the list

## Benefits

+ using your preferred text editor (I use Emacs with PO mode) or PO editor
  (poedet, lokalize)
+ short files
+ first interactions with list members to know the team and his habits/history

# The manpages

+ clone the git repo
+ see the page of stats about translating (2288 pages), 61% - 1400 translated,
  mainly section 2,3 & 7
+ choose a PO file (partially translated or untranslated) and translate it
  (via an ITT message or after discussing with
  the other contributors to ensure not duplication)
+ after translation, request for review, then at the end, push to git or request
someone to do it

## Benefits

+ just translate and discuss
+ German team does the maintenance to upload in the Debian archive

## Documentation

+ Debian software
+ Release notes
+ Debian Developer's Reference
+ etc

## Packages

+ interactive interfaces of the Debian packages (developed by and for the distribution):
+ dpkg
+ apt
+ aptitude
+ …

## Announces

All the messages on debian-news-french :

+ Debian releases
+ DPN
+ miscellaneous announces (bits, etc)

## Website: once we are motivated and if time

All the static part of the website as well as new contents:

+ publicity
+ security announces (DSA, DLA)
+ platform of the DPL candidates
+ …

# For each step, we can do several things according to the level of time and... skills

## Reporting problems

Reporting errors or missing translations:

+ on the debian-l10n-french mailing
+ on IRC

## Tracking translations

Just subscribe to [debian-l10n-french](https://lists.debian.org/debian-l10n-french/)

## Review

Review and fix translations submitted:

+ on the mailing list
+ on the DDTSS (we would love to prioritize new translations, not fixing
  the ones done)

## Translating

+ adopting translations 

+ starting new translations (eg. ddts)

# Conclusion: human experience instead of tools

+ given our capabilities, but also given the status of automated tools from an
  accessibility point of view, we propose a way for a new contributor,
  from the easiest to the less easy, each project has a level of involvement,
  depending on time
+ and we speak the native language of contributor on the list
